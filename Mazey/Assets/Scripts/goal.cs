﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class goal : MonoBehaviour {

    public string nxtLvl;

    Text txt;

    bool goalReached = false;

	void Start () {

        txt = GameObject.FindObjectOfType<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
        if (goalReached == true)
        {
            txt.text = "Level complete!\nTap screen";
            txt.verticalOverflow = VerticalWrapMode.Overflow;
            txt.color = new Color(0,1,0,1);

            if (Input.GetKey(KeyCode.R) || Input.GetButtonUp("Fire1"))
            {
                if (nxtLvl == "")
                {
                    nxtLvl = SceneManager.GetActiveScene().name;
                }
                SceneManager.LoadScene(nxtLvl);
            }
        }
	}

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<playerController>() !=null)
        {
            goalReached = true;
        }
    }
}
