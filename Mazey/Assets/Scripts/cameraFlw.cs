﻿using UnityEngine;
using System.Collections;

public class cameraFlw : MonoBehaviour {

    Transform player;

	void Start () {
        player = GameObject.FindObjectOfType<playerController>().transform;
        transform.rotation = Quaternion.Euler(45,180,0);
	}
	
	void Update () {

        transform.position = new Vector3(player.position.x, player.position.y + 5, player.position.z + 3);
	}
}
