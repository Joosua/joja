﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class playerController : MonoBehaviour {

    #region Attributes
    Rigidbody rb;
    public float speed = 2;
    Text overTxt;
    #endregion

    void Start () {
        rb = GetComponent<Rigidbody>();
        overTxt = GameObject.FindObjectOfType<Text>();
        overTxt.color = new Color(0,0,0,0);
	}
	
	void Update () {
        #region Movement
        pcControls();

        mobileControls();
        #endregion

        #region Screen and Camera stuff
        if (transform.position.y < -2)
        {
            overTxt.text = "Game Over.\nTap screen to try again";
            overTxt.verticalOverflow = VerticalWrapMode.Overflow;
            overTxt.color = new Color(1,0,0,1);

            if (Input.GetKey(KeyCode.R) || Input.GetButtonUp("Fire1")){
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        #endregion
    }

    void pcControls()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.AddForce(new Vector3(speed, 0, 0));
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.AddForce(new Vector3(-speed, 0, 0));
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb.AddForce(new Vector3(0, 0, -speed));
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            rb.AddForce(new Vector3(0, 0, speed));
        }
    }

    void mobileControls()
    {
        rb.AddForce(new Vector3(-Input.acceleration.x, 0, Input.acceleration.z));
    }
}
